function Pokemon(name, level, health, attack)
{
    this.name = name 
    this.level = level
    this.health = health
    this.attack = attack
    this.tackle = function(target) {
        console.log(this.name + " attacked " + target.name)

        target.health -= this.attack

        if(target.health < 5)
        {
            target.faint()
        }else{
            console.log(target.name + "'s health is now " + target.health)
        }
    }
    this.faint = function(){
        console.log(this.name + " fainted.")
    }
}

let charizard = new Pokemon('Charizard', 37, 100, 20)
let blastoise = new Pokemon('Blastoise', 37, 100, 20)
